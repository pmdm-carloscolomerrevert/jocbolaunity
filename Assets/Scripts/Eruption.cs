﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eruption : MonoBehaviour
{

    public GameObject capsula;
    public static int TOTAL_CAPSULAS = 50;
    private int contador;
    public float fireRate = 0.5f;
   

    // Start is called before the first frame update
    void Start()
    {
       StartCoroutine(ThrowStone());
       contador = 0;
    }



    IEnumerator ThrowStone()
    {
        yield return new WaitForSeconds(2.0f);
        do
        {
            Instantiate(capsula, transform.position, Random.rotation);
            contador++;
            print(contador);
            yield return new WaitForSeconds(fireRate);
        } while (contador < TOTAL_CAPSULAS);
    }



    // Update is called once per frame
    void Update()
    {

    }
}
