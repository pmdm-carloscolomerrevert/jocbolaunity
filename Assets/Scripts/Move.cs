using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Move : MonoBehaviour
{
	private static int TOTAL_MONEDAS = 27;
	public float forceValue;
	public float jumpValue;
	private Rigidbody rigidbody;
	private AudioSource audioSalto;
	public AudioClip clipOscuro;
	public AudioClip clipMoneda;
	private AudioSource audioZonaOscura;
	private AudioSource audioMoneda;
	public Light sol;
	public GameObject explosion;
	public GameObject fireMobile;
	private const float yDie = -3.0f;
	private int saltos;
	public static int monedas;
	
    // Start is called before the first frame update
    void Start() {
        rigidbody = GetComponent<Rigidbody>();
        audioSalto = GetComponent<AudioSource>();
        audioZonaOscura = GetComponent<AudioSource>();
        audioMoneda = GetComponent<AudioSource>();
        saltos = 0;
        monedas = 0;
    }

    // Update is called once per frame
    void Update() {

    	if (Input.GetButtonDown ("Jump") && saltos < 1)
        {
	        saltos++;
    		rigidbody.AddForce(Vector3.up * jumpValue, ForceMode.Impulse);
            audioSalto.Play();
    	}

        if (Input.touchCount == 1)
        {
	        if (Input.touches[0].phase == TouchPhase.Began && saltos < 1)
	        {
		        saltos++;
		        rigidbody.AddForce(Vector3.up * jumpValue, ForceMode.Impulse);
		        audioSalto.Play();
	        }	
        }

        if (Mathf.Abs(rigidbody.velocity.y) < 0.01f)
        {
	        saltos = 0;
        }
     
      
        if (transform.position.y < yDie)
        {
	        Destroy(gameObject);
	        Application.LoadLevel("Final");
        }
        
        
    	
    }
    //Para fuerzas constantes
    void FixedUpdate(){

		    rigidbody.AddForce(new Vector3(Input.GetAxis("Horizontal"), 
			                       0,
			                       Input.GetAxis("Vertical")) * forceValue);
        
		    rigidbody.AddForce(new Vector3(Input.acceleration.x, 
			                       0,
			                       Input.acceleration.y) * forceValue);
	    

    	

    }

     void OnCollisionEnter(Collision colision)
    {
	   
		    if (colision.gameObject.tag.Equals("Enemigo"))
		    {
			   
			    Instantiate(explosion, transform.position, Quaternion.identity);
			    Destroy(colision.gameObject);
		    
		    }
		    if (colision.gameObject.tag.Equals("moneda"))
		    {
			    monedas++;
			    Instantiate(fireMobile, transform.position, Quaternion.identity);
			    audioMoneda.PlayOneShot(clipMoneda);
			    Destroy(colision.gameObject);
			    if (monedas == TOTAL_MONEDAS)
			    {
				    Application.LoadLevel("Win");
			    }
		    }
		    
	    
	  
	    
    }

     private void OnTriggerEnter(Collider other)
     {
	     print("Entras en la zona oscura1");
	     sol.intensity = 0;


     }

     private void OnTriggerStay(Collider other)
     {
	     if (!audioZonaOscura.isPlaying )
	     {
		     
		     audioZonaOscura.PlayOneShot(clipOscuro);
	     }
     }

     private void OnTriggerExit(Collider other)
     {
	     sol.intensity = 1;
		audioZonaOscura.Stop();
     }
}
