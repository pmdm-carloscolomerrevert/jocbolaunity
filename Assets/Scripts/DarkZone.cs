﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkZone : MonoBehaviour
{
    private AudioSource audioOscuro;
    // Start is called before the first frame update
    void Start()
    {
        audioOscuro = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
      
    }

    private void OnTriggerExit(Collider other)
    {
       
       audioOscuro.Play();
       
    }
}
